@echo off
echo QEMU Windows Guest Tools Installer Kickstart Script

echo Making directories in Desktop\QEMUGuestTools\
mkdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\"
mkdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\amd64\"
mkdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\bin\"
mkdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\multiarch\"
mkdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\x86\
 
echo Copying files
copy /v /y install_drivers.bat "C:\%HOMEPATH%\Desktop\QEMUGuestTools\" > NUL
copy /v /y amd64\qemu-ga-x64.msi "C:\%HOMEPATH%\Desktop\QEMUGuestTools\amd64\" > NUL
copy /v /y bin\*.* "C:\%HOMEPATH%\Desktop\QEMUGuestTools\bin\" > NUL
copy /v /y multiarch\spice-guest-tools-*.exe "C:\%HOMEPATH%\Desktop\QEMUGuestTools\multiarch\" > NUL
copy /v /y x86\qemu-ga-x86.msi "C:\%HOMEPATH%\Desktop\QEMUGuestTools\x86\" > NUL

echo Starting installer
echo ----------------------------------
start /wait /d "C:\%HOMEPATH%\Desktop\QEMUGuestTools\" install_drivers.bat
echo.

echo Deleting temp files
del /q /f /s "C:%HOMEPATH%\Desktop\QEMUGuestTools\amd64\*.*" > NUL
del /q /f /s "C:%HOMEPATH%\Desktop\QEMUGuestTools\bin\*.*" > NUL
del /q /f /s "C:%HOMEPATH%\Desktop\QEMUGuestTools\multiarch\*.*" > NUL
del /q /f /s "C:%HOMEPATH%\Desktop\QEMUGuestTools\x86\*.*" > NUL
del /q /f /s "C:%HOMEPATH%\Desktop\QEMUGuestTools\*.*" > NUL

echo Deleting empty temp folders
rmdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\amd64"
rmdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\bin"
rmdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\multiarch"
rmdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\x86"
rmdir "C:\%HOMEPATH%\Desktop\QEMUGuestTools\"

echo Cleanup complete
echo Press a key to close this window
pause > NUL
