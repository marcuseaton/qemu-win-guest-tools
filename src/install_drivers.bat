@echo off

echo This will install all Windows Guest VirtIO drivers for QEMU.
echo.
echo --------------------------------------------------------
echo Want to do this process manually?            
echo Close this window and read manual_install.txt on the ISO
echo --------------------------------------------------------
echo.
echo ******DIAGNOSTIC LOG FOLLOWS******
echo I- Information P- Prompt W- Warning E- Error
echo.

cd "C:\%HOMEPATH%\Desktop\QEMUGuestTools\"

REM Ask if user wants to download the latest drivers

:updateprompt

echo I- Would you like to check for updates to the QEMU guest tools?
echo I- This will require a internet connection and approximately 20MB of disk space.

set download=

set /p download=P- (Y/N?) 

if '%download%' == 'Y' goto download
if '%download%' == 'y' goto download

if '%download%' == 'N' goto archdetect
if '%download%' == 'n' goto archdetect

echo.
echo E- Invalid selection, please try again.
echo P- Press a key.
pause > NUL
cls
goto updateprompt

REM Download drivers

:download

echo I- Renaming old downloads

cd amd64
ren qemu-ga-x64.msi qemu-ga-x64.old
cd ..
cd multiarch
ren spice-guest-tools-*.exe spice-guest-tools-*.old
cd ..
cd x86
ren qemu-ga-x86.msi qemu-ga-x86.old
cd ..

echo I- Starting downloads
echo I- Downloading update lists

bin\wget.exe -q --no-check-certificate -O update-qemu-ga-x64.bat https://raw.githubusercontent.com/marcusteaton/qemu-guest-tools-urls/master/update-qemu-ga-x64.bat
bin\wget.exe -q --no-check-certificate -O update-qemu-ga-x86.bat https://raw.githubusercontent.com/marcusteaton/qemu-guest-tools-urls/master/update-qemu-ga-x86.bat
bin\wget.exe -q --no-check-certificate -O update-spice-tools.bat https://raw.githubusercontent.com/marcusteaton/qemu-guest-tools-urls/master/update-spice-tools.bat

echo I- Update lists downloaded
echo I- Downloading newest drivers

call update-qemu-ga-x64.bat
call update-qemu-ga-x86.bat
call update-spice-tools.bat

echo I- Downloads finished, starting installers

REM Since Windows does not allowus to execute filenames with wildcards in it,
REM get current filename, export to a temporary text file, echo file to
REM variable, delete temp text file.

echo I- Getting filename for SPICE installer

cd multiarch
dir /b *.exe > spice-filename.txt
set /p spicefilename= < spice-filename.txt
del spice-filename.txt
cd ..

REM First we check to see if the OS is 32bit or 64bit
REM This environment variable should be set on WinXP or newer by the OS

:archdetect

echo I- Attempting to detect OS architecture

if '%PROCESSOR_ARCHITECTURE%' == 'AMD64' goto InstallAMD64
if '%PROCESSOR_ARCHITECTURE%' == 'x86' goto Installx86

echo E- Could not detect architecture, falling back to user prompt

REM Clear our environment variable so the user can choose

:archprompt
set archprompt=

set /p archprompt=P- Please enter the architecture this guest OS uses, AMD64 for 64-bit, x86 for 32-bit- 

REM If user types the correct architecture, go to that install section, otherwise error out

if '%archprompt%' == 'AMD64' goto InstallAMD64
if '%archprompt%' == 'x86' goto Installx86

echo E- Architecture is not supported or has any QEMU guest tools.
echo Press a key to try again, or close this window to cancel.
pause > NUL
goto archprompt

REM Install section for 64-Bit drivers

:InstallAMD64
echo I- Detected %PROCESSOR_ARCHITECTURE%
echo I- Installing 64-Bit Drivers
echo I- Installing 1 of 2 - QEMU Guest Agent for 64-Bit Guests
amd64\qemu-ga-x64.msi

echo I- Installing 2 of 2 - SPICE Guest Tools for All Guests
multiarch\%spicefilename%

echo I- Installations completed
echo I- REBOOT REQUIRED TO COMPLETE INSTALL

echo P- Press a key to close this window
pause > NUL
goto exit

REM Install section for 32-Bit drivers

:Installx86
echo I- Detected %PROCESSOR_ARCHITECTURE%
echo I- Installing 32-Bit Drivers
echo I- Installing 1 of 2 - QEMU Guest Agent for 32-Bit Guests
x86\qemu-ga-x86.msi

echo I- Installing 2 of 2 - SPICE Guest Tools for All Guests
multiarch\%spicefilename%

echo I- Installations completed
echo I- REBOOT REQUIRED TO COMPLETE INSTALL

echo P- Press a key to close this window
pause > NUL
goto exit

:exit
exit
